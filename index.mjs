import { format } from "@fast-csv/format";
import FormData from "form-data";
import MultiStream from "multistream";

import { Reporter } from "./lib/reporter.mjs";
import { IpDeduplicateTransform } from "./lib/ip-deduplicate-transform.mjs";
import { getPostfixStream, getDovecotStream } from "./lib/streams.mjs";

const dryRun = !!process.argv.find((arg) =>
  ["-d", "--dry-run"].includes(arg.toLowerCase())
);

const postfixStream = getPostfixStream();
const dovecotStream = getDovecotStream();

const stream = MultiStream.obj([
  postfixStream,
  dovecotStream
])
  .pipe(new IpDeduplicateTransform())
  .pipe(format({ headers: true }));

const form = new FormData();
form.append("csv", stream, { filename: "report.csv" });
form.pipe(new Reporter(dryRun, form.getHeaders()));
