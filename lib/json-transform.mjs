import { Transform } from "stream";
import { StringDecoder } from "string_decoder";

export class JsonTransform extends Transform {
  constructor() {
    super({ readableObjectMode: true });
    this._buffer = "";
    this._decoder = new StringDecoder("utf8");
  }

  _transform(chunk, _encoding, cb) {
    const decoded = this._decoder.write(chunk);

    const lines = (this._buffer + decoded).split(/[\r\n]/);

    this._buffer = "";

    const first = lines.slice(0, -1);
    const last = lines[lines.length - 1];

    if (lines.length > 1) {
      first.forEach((line) => {
        if (line.length) {
          try {
            const parsed = JSON.parse(line);
            this.push(parsed);
          } catch (ex) {
            console.dir({ ex, line });
          }
        }
      });
    }

    try {
      const parsed = JSON.parse(last);
      this.push(parsed);
    } catch {
      this._buffer = last;
    }

    cb();
  }
}
