import { Transform } from "stream";

export class IpDeduplicateTransform extends Transform {
  constructor() {
    super({ readableObjectMode: true, writableObjectMode: true });
    this._ipMap = {};
  }

  _transform(chunk, encoding, cb) {
    if (this._ipMap[chunk.IP]) {
      return cb();
    }
    this._ipMap[chunk.IP] = true;

    this.push(chunk, encoding, cb);

    cb();
  }
}
