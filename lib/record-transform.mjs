import { Transform } from "stream";

function parseLogDate(SYSLOG_TIMESTAMP) {
  const timestamp = SYSLOG_TIMESTAMP.trim().split(" ");
  timestamp.splice(2, 0, new Date().getFullYear().toString());
  return new Date(Date.parse(timestamp.join(" "))).toISOString();
}

export class PostfixRecordTransform extends Transform {
  constructor() {
    super({ readableObjectMode: true, writableObjectMode: true });
  }

  _transform(chunk, _encoding, callback) {
    const regexp =
      /NOQUEUE: reject: (RCPT|VRFY) from (?<host>.*)\[(?<ip>.*?)\]: (?<code>\d{3}) (?<error>\d+\.\d+\.\d+) (?<message>.*)/;
    const whitelist = ["spf mfrom check failed"];

    const Categories = "11";
    const { SYSLOG_TIMESTAMP, MESSAGE } = chunk;

    const ReportDate = parseLogDate(SYSLOG_TIMESTAMP);

    const result = MESSAGE.match(regexp);

    if (!result) {
      console.dir({ error: "Could not parse message", message: MESSAGE });
      return callback();
    }

    const { ip, code, message } = result.groups;

    if (code === "450") {
      return callback();
    }

    if (whitelist.find((str) => message.includes(str))) {
      return callback();
    }

    const Comment = message.replaceAll(/<.*?>/g, "<redacted>");

    this.push({ IP: ip, Categories, ReportDate, Comment });

    callback();
  }
}

export class DovecotRecordTransform extends Transform {
  constructor() {
    super({ readableObjectMode: true, writableObjectMode: true });
  }

  _transform(chunk, _encoding, callback) {
    const regexp = /imap-login: Disconnected(?<message1>.*?): user=<(?<user>.*?)>, rip=(?<rip>\d+\.\d+\.\d+\.\d+), lip=(?<lip>\d+\.\d+\.\d+\.\d+),(?<message2>.*?) session=<.*?>/;
    const Categories = "18";
    const { SYSLOG_TIMESTAMP, MESSAGE } = chunk;

    const ReportDate = parseLogDate(SYSLOG_TIMESTAMP);

    const result = MESSAGE.match(regexp);
    if (!result) {
      console.dir({ error: "Could not parse message", message: MESSAGE });
      return callback();
    }

    const { rip, message1 } = result.groups;

    this.push({ IP: rip, Categories, ReportDate, Comment: message1.trim() });

    callback();
  }
}
