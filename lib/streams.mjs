import { spawn } from "child_process";

import { JsonTransform } from "./json-transform.mjs";
import { PostfixRecordTransform, DovecotRecordTransform } from "./record-transform.mjs";

export function getPostfixStream() {
  const proc = spawn("journalctl", [
    "--since",
    "yesterday",
    "--until",
    "today",
    "-u",
    "postfix@-",
    "-t",
    "postfix/smtpd",
    "-g",
    "NOQUEUE:",
    "-o",
    "json",
  ]);

  return proc.stdout
    .pipe(new JsonTransform())
    .pipe(new PostfixRecordTransform());
}

export function getDovecotStream() {
  const proc = spawn("journalctl", [
    "--since",
    "yesterday",
    "--until",
    "today",
    "-u",
    "dovecot",
    "-g",
    "imap-login: Disconnected",
    "-o",
    "json",
  ]);

  return proc.stdout
    .pipe(new JsonTransform())
    .pipe(new DovecotRecordTransform());
}
