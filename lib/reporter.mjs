import { request } from "https";
import { URL, urlToHttpOptions } from "url";
import { Writable } from "stream";
import { strict as assert } from "assert";
import { config } from "dotenv";
config();

const ABUSEIPDB_KEY = process.env.ABUSEIPDB_KEY;
const ABUSEIPDB_ENDPOINT = process.env.ABUSEIPDB_ENDPOINT;
assert(ABUSEIPDB_KEY, "Please provice AbuseIpDb API key");
assert(ABUSEIPDB_ENDPOINT, "Please provice AbuseIpDb endpoint");

export class Reporter extends Writable {
  constructor(dryRun, headers) {
    super({ writableObjectMode: true, readableObjectMode: true });
    const options = {
      ...urlToHttpOptions(new URL(ABUSEIPDB_ENDPOINT)),
      method: "POST",
      headers: {
        "User-Agent": "postfix-abuseipdb/1.0.0",
        Key: ABUSEIPDB_KEY,
        Accept: "application/json",
        ...headers,
      },
    };

    this._outputStream = dryRun
      ? process.stdout
      : request(options, this._responseHandler.bind(this));
  }

  _write(chunk, encoding, callback) {
    return this._outputStream.write(chunk, encoding, callback);
  }

  _final(callback) {
    this._outputStream.end(callback);
  }

  _responseHandler(res) {
    const chunks = [];

    res.on("data", (chunk) => {
      chunks.push(chunk);
    });

    res.on("end", () => {
      const response = chunks.join("");

      console.dir(JSON.parse(response));

    });
  }
}
